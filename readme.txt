=== Eight Sec ===
Contributors: 8degreethemes
Tags: one-column, right-sidebar, full-width-template, custom-logo, custom-menu, custom-background, threaded-comments, translation-ready, portfolio, rtl-language-support
Requires PHP: 5.6
Requires at least: 5.6
Tested up to: 5.4.1
Stable tag: 1.1.4
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Eight Sec theme is a free wordpress one page theme with eight sections. It is ideal for business, corporate, portfolio, blog, agency, freelancer and any other modern and creative website. It has fullwidth and boxed layout and is fully responsive. Demo: http://8degreethemes.com/demo/eight-sec Support forum: support@8degreethemes.com

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

-----------------------------------------------------
JS Files
   
   bxSlider: MIT license
   http://opensource.org/licenses/MIT
      
   WOW JS: MIT license
   https://github.com/matthieua/WOW

   Images Loaded: MIT License
   https://opensource.org/licenses/MIT
   
   Isotope PACKAGED: GNU GPL license v3
   http://isotope.metafizzy.co/#license

   smooth-scroll: MIT license
   http://github.com/cferdinandi/smooth-scroll
   
   
------------------------------------------------------
Fonts
   Font Awesome: MIT and GPL licenses
   http://fontawesome.io/license/
   
   Oswald: Apache License, version 2.0
   https://www.google.com/fonts/specimen/Oswald
   
   Raleway: SIL Open Font License, 1.1
   https://www.google.com/fonts/specimen/Raleway

   Open sans: Apache License, version 2.0
   https://www.google.com/fonts/specimen/Open+Sans
   
------------------------------------------------------
Images
   All the images used are from https://stocksnap.io/ or self taken are fully GPL compatible.
   https://stocksnap.io/photo/35BD398A51
   https://stocksnap.io/photo/B8DPOZMDK7
   https://stocksnap.io/photo/X0R5VP6BSB

-------------------------------------------------------

== Frequently Asked Questions ==
      
== Changelog ==

= 1.1.4 =
** Added Required Header Fields: `Tested up to` and `Requires PHP` in Style.css.
** Added Featured Image Show Hide Option for Single Post
** Fixed Minor CSS and JS issues

= 1.1.3 =
** Added Skip to Link Content 
** Added Read More Button for Team Archive Page & Portfolio Archive Page

= 1.1.2 =
** Removed git url as recommended by the @trtmessenger.

= 1.1.1 =
** Fixed some design issues

= 1.1.0 =
** Removed Demo contents from theme and made to be able to import from plugin from git.
** Files organised and minor fixes.
** Updated images and licenses

= 1.0.23 =
** Added rtl language support
** Refining theme and responsive designs

= 1.0.22 =
** Added gutenberg plugin compability

= 1.0.21 =
** fixed responsive sticky menu
** bind all search settings of header in customizer
** removed un-used welcome folder

= 1.0.20 =
** added additional wordpress resources links

= 1.0.19 =
** fixed header issue in responsive
** shortcode compatiable in homepage section

= 1.0.18 =
** fixed wpml and polylang translation for page and category option in customizer
** Fixed sticky menu design in responisive
      
= 1.0.17 =
** Fixed Menu Issue on normal menu.

= 1.0.16 =
** escaping variables and strings
** maintain indentation
** added wpml-config.xml file for string translations
** renamed Homepage Setting panel name to Homepage Section

= 1.0.15 =
** added custom header option to add background image in header
** theme tested upto wordpress version 4.9

= 1.0.14 =
** added demo import functionality in welcome page.

= 1.0.13 =
** fixed menu bugs
** added imagesload to fix portfolio images load in portfolio

= 1.0.12 =
** fixed in minor issue
** small changes in design of portfolio hover content in homepage

= 1.0.11 =
** links not working for other language links fixed

= 1.0.10 =
** skype link fixed
** added onepage menu in innerpages

= 1.0.09 =
** changing the section id dynamically.
** search page design issue fixed.

= 1.0.08 =
** Fix of slider image in iphone,imac browsers.
** Minor design fixes.

= 1.0.07 =
** modifying starter content

= 1.0.06 =
** solved header design issue
** corrected mis-escaping

= 1.0.05 =
** solved issues on archive page
** chaning escaping for portfolio readmore button text
** modifying homepage sections


= 1.0.04 =
** supports wordpres4.7 edit icon in customizer
** removed text title and description and added dropdown pages in customizer for homepage sections
** removed eight_sec_excerpt and eight_sec_sanitize_text function and used the_expert and sanitize_text_field instead
** escaped and translated untrusted data and text
** removed used of esc_textarea

= 1.0.03 =
** fixed recommended issues
** fixed readmore button text issues
** fixed design issues for mobile

= 1.0.02 =
** used the_post_thumbnail function on single page and post
** added link to the title of the sevice posts on about section
** fixed readmore button text issues
** fixed design issues for mobile
** removed custom header
** footer style changed
** change in search box alignment
** fixed bugs

= 1.0.01 =
** fixed bugs and minor changes

= 1.0.0 =
** Submitted theme to wordpress.org
* Initial release